<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
<div class="close-menu">
    <a on="tap:sidebar.toggle">
    <img src="app/views/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
</div>
<a href="/dashboard"><img src="app/views/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
<div>
    <ul>
    <li><a href="/categorias" class="link-menu">Categorias</a></li>
    <li><a href="/produtos" class="link-menu">Produtos</a></li>
    </ul>
</div>
</amp-sidebar>