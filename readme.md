# Webjump backend assessment
Repo containing my code to [Webjump backend assessment](https://bitbucket.org/webjump/assessment-backend-xp/src/master/).

## Structure decisions
I decided to setup a simple micro-framework to build this "app", using Eloquent as my ORM, Blade as my render engine, Dotenv to facilitate environment setup  and a Router [i wrote for my own micro-framework](github.com/fernando-sa/fernWork/).

Using MVC and repository patterns, I aimed on doing thin controller methods and reusable database queries, while keeping a future change of database drivers an option. I decided on doing manual validation, since i never did it and thought it was a good chance to learn more about the validation process.

The server is served by index.php but the actual framework code runs on framework/App.php.

There are 2 composer packages only in dev environment, phpUnit for testing and codeSniffer for PSR-2 formatting.

## Installation
This assume you're using linux.

1. To run this project in your computer you need php 7^, composer and a MySql database already created.

3. Install dependecies with composer install.

3. Create an .env from the .env.example file and put your db credentials.

4. Once .env was configured, you can set `./buildDatabase.sh` to be executable and run it, so all the tables are setup properly.
5. To run the server you can use php own server, with `php -S localhost:8000`

## Testing
Tests cover all CRUD operations in the repository. It would be nice to have feature tests so we can deal with controller testing, but it was not done.

To run tests, run phpunit with `./vendor/bin/phpunit ./tests/unit/`

## What I would do different
Not using an actual validation library was ultra bad, since manual validation is completely impractical.

Not having a helper class or library to format responses and deal with that was not good. It also lead to blank pages when an error ocurred.

I didn't setup test to use a different env, which is obviously really bad.

---
Thank you :)

 
