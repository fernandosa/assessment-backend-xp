<?php

namespace App\controllers;

use App\database\repositories\mysqlEloquent\CategoriesRepository;
use App\formValidation\CategoryValidator;
use Exception;
use Framework\views\Blade;

class CategoryController
{

    private $categoriesRepository;

    public function __construct()
    {
        $this->categoriesRepository = new CategoriesRepository;
    }

    public function index(): string
    {
        $categories = $this->categoriesRepository->getAllCategories();
        return Blade::render('views.categories.categories', ['categories' => $categories]);
    }

    public function showCreate() : string
    {
        return Blade::render('views.categories.addCategory');
    }

    public function create()
    {
        $postRequest = $_POST;
        
        if ($errors = CategoryValidator::validate($postRequest)) {
            return Blade::render('views.categories.addCategory', ['error' => $errors]);
        }

        try {
            $this->categoriesRepository->create($postRequest);
        } catch (Exception $e) {
            return Blade::render('views.categories.addCategory', ['error' => "Problema ao criar a categoria!"]);
        }
        header('Location: ' . $_ENV['APP_URL'] . '/categorias');
        exit();
    }

    public function showEdit(int $categoryId)
    {
        $category = $this->categoriesRepository->find($categoryId);
        return Blade::render('views.categories.editCategory', ['category' => $category]);
    }
    
    public function edit(int $categoryId)
    {
        $postRequest = $_POST;

        if ($errors = CategoryValidator::validate($postRequest)) {
            return Blade::render(
                'views.categories.editCategory',
                ['error' => $errors, 'category' => $this->categoriesRepository->find($categoryId)]
            );
        }

        $this->categoriesRepository->edit($categoryId, $postRequest);
        header('Location: ' . $_ENV['APP_URL'] . '/categorias');
        exit();
    }

    public function delete(int $categoryId)
    {
        $category = $this->categoriesRepository->find($categoryId);
        // We remove all links between this category and products
        $category->products()->detach();
        
        $this->categoriesRepository->delete($categoryId);
        header('Location: ' . $_ENV['APP_URL'] . '/categorias');
        exit();
    }
}
