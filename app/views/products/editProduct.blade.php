@extends('views.layouts.master')
@section('content')
  <h1 class="title new-item">Edit Product</h1>

  <form method="POST" action="/editar-produto/{{$product->id}}">
    <div class="input-field">
      <label for="sku" class="label">Product SKU</label>
      <input type="text" name="sku" value="{{$product->sku}}" id="sku" class="input-text" /> 
    </div>
    <div class="input-field">
      <label for="name" class="label">Product Name</label>
      <input type="text" name="name" value="{{$product->name}}" id="name" class="input-text" /> 
    </div>
    <div class="input-field">
      <label for="price" class="label">Price</label>
      <input type="text" name="price" value="{{$product->price}}" id="price" class="input-text" /> 
    </div>
    <div class="input-field">
      <label for="quantity" class="label">Quantity</label>
      <input type="text" name="quantity" value="{{$product->quantity}}" id="quantity" class="input-text" /> 
    </div>
    <div class="input-field">
      <label for="category" class="label">Categories</label>
      <select multiple name="category[]" id="category" class="input-text">
        @foreach ($categories as $category)
            <option value="{{ $category->id }}"
                @if(in_array($category->id, $product->categories()->pluck('categoryId')->toArray())) selected @endif >
                {{ $category->name }}
            </option>
        @endforeach
  
      </select>
    </div>
    <div class="input-field">
      <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text">{{$product->description}}</textarea>
    </div>
    <div class="actions-form">
      <a href="products.html" class="action back">Back</a>
      <input class="btn-submit btn-action" type="submit" value="Save Product" />
    </div>
    
  </form>
@endsection