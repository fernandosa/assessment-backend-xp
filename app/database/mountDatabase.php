<?php

require __DIR__ . './../../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Framework\App;

// We need a new app instance to connect with db.
$app = new App;
$app->setupDotEnv();
$app->setupDatabase();

if (! Capsule::schema()->hasTable('products')) {
    Capsule::schema()->create('products', function ($table) {
        $table->bigIncrements('id');
        $table->string('name', 255);
        $table->string('sku', 20)->unique();
        $table->double('price', 7, 2);
        $table->text('description');
        $table->integer('quantity')->unsigned();
        $table->timestamps();
    });
};

if (! Capsule::schema()->hasTable('categories')) {
    Capsule::schema()->create('categories', function ($table) {
        $table->bigIncrements('id');
        $table->string('name', 255);
        $table->string('code', 20)->unique();
        $table->timestamps();
    });
};

if (! Capsule::schema()->hasTable('category_product')) {
    Capsule::schema()->create('category_product', function ($table) {
        $table->bigIncrements('id');
        $table->bigInteger('productId')->unsigned();
        $table->bigInteger('categoryId')->unsigned();
        $table->foreign('productId')->references('id')->on('products');
        $table->foreign('categoryId')->references('id')->on('categories');
        $table->timestamps();
    });
};
