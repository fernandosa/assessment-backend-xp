<?php

namespace App\database\repositories\interfaces;

use App\models\Product;
use Illuminate\Database\Eloquent\Collection;

interface IProductsRepository
{
    public function create(array $args) : Product;
    public function getAllProducts() : Collection;
    public function find(int $productId) : Product;
    public function delete(int $productId) : void;
    public function edit(int $id, array $parameters) : Product;
}
