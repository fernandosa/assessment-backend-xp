@extends('views.layouts.master')
@section('content')
<div class="header-list-page">
    <h1 class="title">Products</h1>
    <a href="/adicionar-produto" class="btn-action">Add new Product</a>
  </div>
  <table class="data-grid">
    <tr class="data-row">
      <th class="data-grid-th">
          <span class="data-grid-cell-content">Name</span>
      </th>
      <th class="data-grid-th">
          <span class="data-grid-cell-content">SKU</span>
      </th>
      <th class="data-grid-th">
          <span class="data-grid-cell-content">Price</span>
      </th>
      <th class="data-grid-th">
          <span class="data-grid-cell-content">Quantity</span>
      </th>
      <th class="data-grid-th">
          <span class="data-grid-cell-content">Categories</span>
      </th>

      <th class="data-grid-th">
          <span class="data-grid-cell-content">Actions</span>
      </th>
    </tr>
    @foreach ($products as $product)
    <tr class="data-row">
      <td class="data-grid-td">
          <span class="data-grid-cell-content">{{ $product->name }}</span>
      </td>
    
      <td class="data-grid-td">
          <span class="data-grid-cell-content">{{ $product->sku }}</span>
      </td>

      <td class="data-grid-td">
          <span class="data-grid-cell-content">R$ {{ $product->price }}</span>
      </td>

      <td class="data-grid-td">
          <span class="data-grid-cell-content">{{ $product->quantity }}</span>
      </td>

      <td class="data-grid-td">
          <span class="data-grid-cell-content">
            @foreach ($product->categories as $category)
                {{ $category->name }}
                <br>
            @endforeach
          </span>
      </td>
      
      <td class="data-grid-td">
        <div class="actions">
          <div class="action edit"><a href="/editar-produto/{{ $product->id }}">Edit</a></div>
          <div class="action delete"><a href="/deletar-produto/{{ $product->id }}">Delete</a></div>
        </div>
      </td>
    </tr>
    @endforeach
  </table>
@endsection
