<?php

namespace App\database\repositories\interfaces;

use App\models\Category;
use Illuminate\Database\Eloquent\Collection;

interface ICategoriesRepository
{
    public function create(array $args) : Category;
    public function getAllCategories() : Collection;
    public function getFirstCategory() : Category;
    public function find(int $id);
    public function edit(int $id, array $parameters) : Category;
    public function delete(int $id) : void;
}
