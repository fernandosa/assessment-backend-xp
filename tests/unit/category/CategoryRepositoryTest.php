<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;


use App\database\repositories\mysqlEloquent\CategoriesRepository;
use App\formValidation\CategoryValidator;
use App\models\Category;
use Framework\App;

final class CategoryCRUDTest extends TestCase
{
    public function testCreateCategory(): void
    {
        $app = new App;
        $app->setupDotEnv();
        $app->setupDatabase();

        $categoriesRepository = new CategoriesRepository;

        $data['name'] = $this->generateRandomString(10);
        $data['code'] = $this->generateRandomString(10);

        $return = $categoriesRepository->create($data);
        $this->assertInstanceOf(Category::class, $return);

    }

    public function testCanEdit(): void
    {
        $app = new App;
        $app->setupDotEnv();
        $app->setupDatabase();

        $categoriesRepository = new CategoriesRepository;

        $data['name'] = $this->generateRandomString(10);
        $data['code'] = $this->generateRandomString(10);

        $category = $categoriesRepository->create($data);

        $data['name'] = $this->generateRandomString(10);
        $data['code'] = $this->generateRandomString(10);

        $category = $categoriesRepository->edit($category->id, $data);
        $this->assertInstanceOf(Category::class, $category);

        $this->assertEquals($category->name, $data['name']);
        $this->assertEquals($category->code, $data['code']);

    }

    public function testCanDelete(): void
    {
        $app = new App;
        $app->setupDotEnv();
        $app->setupDatabase();

        $categoriesRepository = new CategoriesRepository;

        $data['name'] = $this->generateRandomString(10);
        $data['code'] = $this->generateRandomString(10);

        $category = $categoriesRepository->create($data);

        $categoriesRepository->delete($category->id);
        $this->assertNull($categoriesRepository->find($category->id));

    }

    public function testCategoryValidation(): void
    {
        $app = new App;
        $app->setupDotEnv();
        $app->setupDatabase();

        $categoriesRepository = new CategoriesRepository;

        $data['name'] = "";
        $data['code'] = $this->generateRandomString(10);

        $errors = CategoryValidator::validate($data);
        $this->assertIsString($errors);

        $data['name'] = $this->generateRandomString(10);
        $data['code'] = "";

        $errors = CategoryValidator::validate($data);
        $this->assertIsString($errors);

    }

    private function generateRandomString(int $length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}