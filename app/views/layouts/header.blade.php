<div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="/dashboard" class="link-logo"><img src="app/views/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
</div>
<div class="right-box">
    <span class="go-title">Administration Panel</span>
</div>
<div style="text-align:center;">
    <br>
    <div style="color: red">
        @if (isset($error))
            {!! $error !!}
        @endif
    </div>
    <div style="color: green">
        @if (isset($success))
            {!! $success !!}
        @endif
    </div>
</div>