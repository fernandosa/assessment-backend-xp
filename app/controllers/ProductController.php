<?php

namespace App\controllers;

use App\database\repositories\mysqlEloquent\CategoriesRepository;
use App\database\repositories\mysqlEloquent\ProductsRepository;
use App\formValidation\ProductValidator;
use App\models\Product;
use Exception;
use Framework\views\Blade;

class ProductController
{
    
    private $categoriesRepository;
    private $productsRepository;

    public function __construct()
    {
        $this->categoriesRepository = new CategoriesRepository;
        $this->productsRepository = new ProductsRepository;
    }

    public function index()
    {
        $products = $this->productsRepository->getAllProducts();
        return Blade::render('views.products.products', ['products' => $products]);
    }

    public function showCreate()
    {
        $categories = $this->categoriesRepository->getAllCategories();
        return Blade::render('views.products.addProduct', ['categories' => $categories]);
    }

    public function create()
    {
        $postRequest = $_POST;

        if ($errors = ProductValidator::validate($postRequest)) {
            $categories = $this->categoriesRepository->getAllCategories();
            return Blade::render('views.products.addProduct', ['error' => $errors, 'categories' => $categories]);
        }

        try {
            $this->productsRepository->create($postRequest);
        } catch (Exception $e) {
            return Blade::render('views.products.addProduct', ['error' => "Problema ao criar o produto!"]);
        }
        
        header('Location: ' . $_ENV['APP_URL'] . '/produtos');
        exit();
    }

    public function showEdit(int $productId)
    {
        $product = $this->productsRepository->find($productId);
        $categories = $this->categoriesRepository->getAllCategories();

        return Blade::render('views.products.editProduct', ['product' => $product, 'categories' => $categories]);
    }

    public function edit(int $productId)
    {
        $postRequest = $_POST;

        if ($errors = ProductValidator::validate($postRequest)) {
            $product = $this->productsRepository->find($productId);
            $categories = $this->categoriesRepository->getAllCategories();
            return Blade::render('views.products.editProduct', ['error' => $errors, 'product' => $product, 'categories' => $categories]);
        }

        $this->productsRepository->edit($productId, $postRequest);

        header('Location: ' . $_ENV['APP_URL'] . '/produtos');
    }

    public function delete(int $productId)
    {
        $this->productsRepository->delete($productId);
        header('Location: ' . $_ENV['APP_URL'] . '/produtos');
    }
}
