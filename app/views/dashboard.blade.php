@extends('views.layouts.master')
@section('content')
<div class="header-list-page">
  <h1 class="title">Dashboard</h1>
</div>
<div class="infor">
  You have {{ $products->count() }} products added on this store: <a href="/adicionar-produto" class="btn-action">Add new Product</a>
</div>
<ul class="product-list">
  @foreach ($products as $product)
  <li>
    <div class="product-image">
      <img src="app/views/images/product/tenis-runner-bolt.png" layout="responsive" width="164" height="145" alt="{{$product->name}}" />
    </div>
    <div class="product-info">
      <div class="product-name"><span>{{ $product->name }}</span></div>
      <div class="product-price"><span class="special-price">{{ $product->quantity }} available</span> <span>R${{ $product->price }}</span></div>
    </div>
  </li>
  @endforeach
</ul>

@endsection
