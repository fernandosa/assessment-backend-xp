<?php

namespace App\formValidation;

class CategoryValidator
{
    public static function validate(array $parameters)
    {
        $errors = "";
        
        // Name validation
        if (! isset($parameters['name']) || $parameters['name'] == "") {
            $errors .= "É necessario adicionar um nome para a categoria<br>";
        } else {
            if (strlen($parameters['name']) > 255) {
                $errors .= "O nome deve ter no máximo 255 caracteres<br>";
            }
        }

        // Code validation
        if (! isset($parameters['code']) || $parameters['code'] == "") {
            $errors .= "É necessario adicionar um código para a categoria<br>";
        } else {
            if (strlen($parameters['code']) > 20) {
                $errors .= "O código deve ter no máximo 20 caracteres<br>";
            }
        }
        if ($errors === '') {
            return false;
        }
        return $errors;
    }
}
