<?php

namespace Framework\Views;

use Jenssegers\Blade\Blade as BladeLibrary;

class Blade
{

    public static function render(string $file, array $parameters = []) : string
    {
        $blade = new BladeLibrary('app', 'storage/cache/views');
        return $blade->render($file, $parameters);
    }
}
