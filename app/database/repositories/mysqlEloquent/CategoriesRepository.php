<?php

namespace App\database\repositories\mysqlEloquent;

use App\database\repositories\interfaces\ICategoriesRepository;
use App\models\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoriesRepository implements ICategoriesRepository
{
    public function create(array $args) : Category
    {
        return Category::create($args);
    }

    public function getAllCategories() : Collection
    {
        return Category::get();
    }

    public function getFirstCategory() : Category
    {
        return Category::first();
    }

    public function find(int $id)
    {
        return Category::find($id);
    }

    public function edit(int $id, array $parameters) : Category
    {
        $category = Category::find($id);
        $category->name = $parameters['name'];
        $category->code = $parameters['code'];
        $category->save();
        return $category;
    }

    public function delete(int $id) : void
    {
        Category::destroy($id);
    }
}
