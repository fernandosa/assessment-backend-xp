<?php

namespace Framework;

use Dotenv\Dotenv;
use Framework\database\Connector;
use Framework\router\Router;

class App
{
    private $dotenv;
    private $database;
    private $router;

    public function setupDotEnv()
    {
        $this->dotenv = Dotenv::createImmutable(__DIR__ . "./../");
        $this->dotenv->load();
    }

    public function setupDatabase()
    {
        $this->database = new Connector;
        $this->database->initiate();
    }

    public function setupRouter()
    {
        $path_info = $_SERVER['PATH_INFO'] ?? '/';
        $request_method = $_SERVER['REQUEST_METHOD'] ?? 'GET';

        $this->router = new Router($path_info, $request_method);
        $this->router->parseRoutesFiles();
    }

    public function run()
    {
        $routeResult = $this->router->run();
        $function = new \ReflectionFunction($routeResult['callback']);

        print_r(call_user_func($function->getClosure(), ... array_values($routeResult['parameters'])));
    }
}
