@extends('views.layouts.master')
@section('content')
<h1 class="title new-item">Edit Category</h1>
    
  <form action="/editar-categoria/{{$category->id}}" method="POST">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input name="name" type="text" value="{{ $category->name }}" id="category-name" class="input-text" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input name="code" type="text" value="{{ $category->code }}" id="category-code" class="input-text" />
        
      </div>
      <div class="actions-form">
        <a href="categories.html" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
@endsection
