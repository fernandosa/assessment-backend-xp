<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use App\database\repositories\mysqlEloquent\CategoriesRepository;
use App\database\repositories\mysqlEloquent\ProductsRepository;
use App\formValidation\ProductValidator;
use App\models\Product;
use Framework\App;

final class ProductRepositoryTest extends TestCase
{
    public function testCreateProduct(): void
    {
        $app = new App;
        $app->setupDotEnv();
        $app->setupDatabase();

        $productsRepository = new ProductsRepository;
        $categoriesRepository = new CategoriesRepository;

        $data['name']        = $this->generateRandomString(10);
        $data['sku']         = $this->generateRandomString(10);
        $data['price']       = rand(1000,2000)/10;
        $data['quantity']    = rand(0,150);
        $data['description'] = $this->generateRandomString(200);
        $data['category'][0] = $categoriesRepository->getFirstCategory()->id;

        $return = $productsRepository->create($data);

        $this->assertInstanceOf(Product::class, $return);

    }

    public function testCanEdit(): void
    {
        $app = new App;
        $app->setupDotEnv();
        $app->setupDatabase();

        $productsRepository = new ProductsRepository;
        $categoriesRepository = new CategoriesRepository;

        $data['name']        = $this->generateRandomString(10);
        $data['sku']         = $this->generateRandomString(10);
        $data['price']       = rand(1000,2000)/10;
        $data['quantity']    = rand(0,150);
        $data['description'] = $this->generateRandomString(200);
        $data['category'][0] = $categoriesRepository->getFirstCategory()->id;

        $product = $productsRepository->create($data);

        $data['name']        = $this->generateRandomString(10);
        $data['sku']         = $this->generateRandomString(10);
        $data['price']       = rand(1000,2000)/10;
        $data['quantity']    = rand(0,150);
        $data['description'] = $this->generateRandomString(200);
        $data['category'][0] = $categoriesRepository->getFirstCategory()->id;

        $product = $productsRepository->edit($product->id, $data);
        $this->assertInstanceOf(Product::class, $product);

        $this->assertEquals($product->name, $data['name']);
        $this->assertEquals($product->sku, $data['sku']);
        $this->assertEquals($product->price, $data['price']);
        $this->assertEquals($product->quantity, $data['quantity']);
        $this->assertEquals($product->description, $data['description']);

    }

    public function testCanDelete()
    {
        $app = new App;
        $app->setupDotEnv();
        $app->setupDatabase();

        $productsRepository = new ProductsRepository;
        $categoriesRepository = new CategoriesRepository;

        $data['name']        = $this->generateRandomString(10);
        $data['sku']         = $this->generateRandomString(10);
        $data['price']       = rand(1000,2000)/10;
        $data['quantity']    = rand(0,150);
        $data['description'] = $this->generateRandomString(200);
        $data['category'][0] = $categoriesRepository->getFirstCategory()->id;

        $product = $productsRepository->create($data);

        $productsRepository->delete($product->id);

        $this->assertNull($categoriesRepository->find($product->id));

    }

    // Test product validation
    
    private function generateRandomString(int $length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}