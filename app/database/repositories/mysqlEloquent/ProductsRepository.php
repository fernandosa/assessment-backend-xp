<?php

namespace App\database\repositories\mysqlEloquent;

use App\database\repositories\interfaces\IProductsRepository;
use App\models\Product;
use Illuminate\Database\Eloquent\Collection;

class ProductsRepository implements IProductsRepository
{
    public function create(array $args) : Product
    {
        $product = Product::create([
            'name' => $args['name'],
            'sku' => $args['sku'],
            'description' => $args['description'],
            'price' => $args['price'],
            'quantity' => $args['quantity'],
        ]);

        try {
            $product->categories()->attach($args['category']);
        } catch (\Throwable $th) {
            $this->delete($product->id);
            throw $th;
        }

        return $product;
    }

    public function getAllProducts() : Collection
    {
        return Product::all();
    }

    public function delete(int $productId) : void
    {
        $product = $this->find($productId);
        $product->categories()->detach();
        Product::destroy($productId);
    }

    public function find(int $productId) : Product
    {
        return Product::find($productId);
    }

    public function edit(int $id, array $parameters) : Product
    {
        $product = Product::find($id);
        $product->name = $parameters['name'];
        $product->sku = $parameters['sku'];
        $product->description = $parameters['description'];
        $product->quantity = $parameters['quantity'];
        $product->price = $parameters['price'];

        $product->categories()->detach();
        $product->categories()->attach($parameters['category']);

        $product->save();

        return $product;
    }
}
