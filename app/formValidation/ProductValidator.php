<?php

namespace App\formValidation;

class ProductValidator
{
    public static function validate(array $parameters)
    {
        $errors = "";

        // Name validation
        if (! isset($parameters['name']) || $parameters['name'] == "") {
            $errors .= "É necessario adicionar um nome para o produto<br>";
        } elseif (strlen($parameters['name']) > 255) {
            $errors .= "O nome deve ter no máximo 255 caracteres<br>";
        }

        if (! isset($parameters['sku']) || $parameters['sku'] == "") {
            $errors .= "É necessario adicionar um código para o produto<br>";
        } else {
            if (strlen($parameters['sku']) > 20) {
                $errors .= "O código deve ter no máximo 20 caracteres<br>";
            }
        }

        if (! isset($parameters['price']) || $parameters['price'] == "") {
            $errors .= "É necessario adicionar um preço para o produto<br>";
        } elseif ($parameters['price'] > 99999.99) {
                $errors .= "O preço deve ser no máximo R$99999.99<br>";
        }

        if (! isset($parameters['description']) || $parameters['description'] == "") {
            $errors .= "É necessario adicionar uma descrição para o produto<br>";
        }

        if (! isset($parameters['quantity']) || $parameters['quantity'] == "") {
            $errors .= "É necessario adicionar uma quantidade para o produto<br>";
        } elseif (! is_numeric($parameters['quantity'])) {
            $errors .= "A quantidade deve ser um número<br>";
        }

        if ($errors === '') {
            return false;
        }
        return $errors;
    }
}
