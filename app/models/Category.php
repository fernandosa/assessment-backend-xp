<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany('App\models\Product', 'category_product', 'categoryId', 'productId');
    }
}
