<?php

require __DIR__ . '/vendor/autoload.php';

use Framework\App;

$app = new App();

$app->setupDotEnv();
$app->setupDatabase();
$app->setupRouter();

$app->run();
