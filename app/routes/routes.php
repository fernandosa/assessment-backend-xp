<?php

$this->get('/dashboard', 'DashboardController@index');


$this->get('/produtos', 'ProductController@index');
$this->get('/adicionar-produto', 'ProductController@showCreate');
$this->post('/adicionar-produto', 'ProductController@create');
$this->get('/editar-produto/{productId}', 'ProductController@showEdit');
$this->post('/editar-produto/{productId}', 'ProductController@edit');
$this->get('/deletar-produto/{productId}', 'ProductController@delete');

$this->get('/categorias', 'CategoryController@index');
$this->get('/adicionar-categoria', 'CategoryController@showCreate');
$this->post('/adicionar-categoria', 'CategoryController@create');
$this->get('/editar-categoria/{categoryId}', 'CategoryController@showEdit');
$this->post('/editar-categoria/{categoryId}', 'CategoryController@edit');
$this->get('/deletar-categoria/{categoryId}', 'CategoryController@delete');
