<?php

namespace Framework\Database;

use Exception;
use Illuminate\Database\Capsule\Manager as Capsule;

class Connector
{

    public function initiate() : void
    {
        // Database layer and ORM loading
        $capsule = new Capsule;

        $capsule->addConnection([
            'driver'    => $_ENV['DB_DRIVER'],
            'host'      => $_ENV['DB_HOST'],
            'database'  => $_ENV['DB_NAME'],
            'username'  => $_ENV['DB_USERNAME'],
            'password'  => $_ENV['DB_PASSWORD'],
            'charset'   => $_ENV['DB_CHARSET'],
            'collation' => $_ENV['DB_COLLATION'],
            'prefix'    => $_ENV['DB_PREFIX'],
        ]);

        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        try {
            $capsule->connection()->getPdo();
        } catch (Exception $e) {
            throw new Exception("Failed to load database. Error: {$e}");
        }
    }
}
