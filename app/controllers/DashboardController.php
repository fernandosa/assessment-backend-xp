<?php

namespace App\controllers;

use App\database\repositories\mysqlEloquent\ProductsRepository;
use Framework\views\Blade;

class DashboardController
{
    private $productsRepository;

    public function __construct()
    {
        $this->productsRepository = new ProductsRepository;
    }

    public function index()
    {
        $products = $this->productsRepository->getAllProducts();
        return Blade::render('views.dashboard', ['products' => $products]);
    }
}
